# Nexus As a Docker Container 

Deploy Nexus as a Docker Container 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Create and Configure Droplet

* Set up and run Nexus as a Docker container

## Technologies Used

* Docker 

* Nexus 

* DigitalOcean

* Linux

## Steps  

Step 1: Create Droplet(server) on DigitalOcean 

[Droplet](/images/01_created_droplet.png)

Step 2: Configure Network firewall by opening ssh port 22 to your ip address and add your droplet created to the firewall configuration 

[Firewall Configuration](/images/02_adding_droplet_to_firewall_configuration.png)
[Access on port 22](/images/03_can_be_accessed_on_port22.png)

Step 3: Ssh into cloud server from Client 

    ssh root@159.65.80.57

[Ssh into Cloud Server](/images/04_ssh_into_cloud_server.png)

Step 4: Install Docker on Cloud server 

    snap install doker 

[Installing docker](/images/05_installing_docker_on_server.png)


Step 5: Create docker volume on cloud server 

    docker volume create --name nexus-data 

[Creating Docker Volume](/images/06_creating_docker_volume.png)

Step 6: Run nexus as a docker container 

    docker run -d -p 8081:8081 --name nexus -v nexus-data:/nexus-data sonatype/nexus3

[Run nexus container](/images/07_running_nexus_as_a_docker_container.png)

Step 7: Install net-tools to be able to have access to netstat to check active ports 

    apt install net-tools 

[Installing net-tools](/images/08_installing_net_tools.png)

Step 8: Check active ports to see if nexus is active 

    netstat -lpnt 

[Nexus Active Ports](/images/09_checking_nexus_container_port.png)

Step 9: Check Nexus UI on the browser localhost:8081

[Nexus UI](/images/10_nexus_ui.png)

Step 10: Check and verify if nexus user is in container 

    docker exec -it a40dab7976e0 /bin/bash 

[Nexus User](/images/11_verifying_nexus_user_in_container.png)

## Installation

* $ snap install docker 

* $ apt install net-tools 

## Usage 

docker run -d -p 8081:8081 --name nexus -v nexus-data:/nexus-data sonatype/nexus3

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/nexus-as-a-docker-container.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review


## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/nexus-as-a-docker-container

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.